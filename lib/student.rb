class Student

  attr_reader :first_name, :last_name

  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    raise if @courses.any? { |course| course.conflicts_with?(new_course)}
    return if @courses.include?(new_course)
    @courses.push(new_course)
    new_course.students.push(self) unless new_course.students.include?(self)
  end

  def course_load
    h = Hash.new(0)
    @courses.each do |course|
      h[course.department] += course.credits
    end
    h
  end

  def has_conflict?(course)

  end
end
